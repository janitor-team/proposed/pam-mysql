From: Helmut Grohne <helmut@subdivi.de>
Date: Sun, 25 Oct 2020 15:10:00 +0100
Subject: Fix FTCBFS: fails finding mysql libraries

pam-mysql fails to cross build from source, because it fails finding
mysql libraries. It tries to do so using mysql_config, but mysql_config
does not work during cross compilation at all. Instead, pkg-config
should be used. This patch implements pkg-config-based detection
and inserts it after mysql_config, but before searching hard coded
locations. As such any previous detection is retained while supporting
cross compilation via pkg-config.

Closes: #972891
---
 m4/pam-mysql.m4 | 126 ++++++++++++++++++++++++++++++--------------------------
 1 file changed, 68 insertions(+), 58 deletions(-)

diff --git a/m4/pam-mysql.m4 b/m4/pam-mysql.m4
index 7b1b583..32c0674 100644
--- a/m4/pam-mysql.m4
+++ b/m4/pam-mysql.m4
@@ -163,84 +163,94 @@ AC_DEFUN([PAM_MYSQL_CHECK_LIBMYSQLCLIENT], [
   AC_MSG_CHECKING([if] $1 [is a mysql_config script])
 
   _cfg="$1"
-  if test -x "$_cfg" -a -r "$_cfg" -a -f "$_cfg"; then
+  AS_IF([test -x "$_cfg" -a -r "$_cfg" -a -f "$_cfg"],[
     dnl $1 may be a path to mysql_config
     AC_MSG_RESULT([yes])
     AC_DEFINE([HAVE_MYSQL_H], [1], [Define to `1' if you have the <mysql.h> header file.])
     mysql_config="$1"
-  else
+  ],[
     AC_MSG_RESULT([no])
-    mysql_lib_path=
-    mysql_include_path=
-    mysql_lib_name=mysqlclient
-
-    for _pfx in $1; do
-      _cfg="$_pfx/bin/mysql_config"
 
-      AC_MSG_CHECKING([mysql_config availability in $_pfx/bin])
+    PKG_CHECK_MODULES([MYSQLCLIENT],[mysqlclient],[
+      mysql_pkg_config=yes
+      AC_DEFINE([HAVE_MYSQL_H], [1], [Define to `1' if you have the <mysql.h> header file.])
+    ],[
+      mysql_pkg_config=no
+      mysql_lib_path=
+      mysql_include_path=
+      mysql_lib_name=mysqlclient
 
-      if test -x "$_cfg" -a -r "$_cfg" -a -f "$_cfg"; then
-        AC_MSG_RESULT([yes])
-        AC_DEFINE([HAVE_MYSQL_H], [1], [Define to `1' if you have the <mysql.h> header file.])
-        mysql_config="$_cfg"
-        break
-      else
-        AC_MSG_RESULT([no])
-      fi
+      for _pfx in $1; do
+        _cfg="$_pfx/bin/mysql_config"
 
-      for dir in "$_pfx/lib" "$_pfx/lib/mysql"; do
-        AC_MSG_CHECKING([$mysql_lib_name availability in $dir])
-        name="$mysql_lib_name"
+        AC_MSG_CHECKING([mysql_config availability in $_pfx/bin])
 
-        if eval test -e "$dir/$libname_spec$shrext_cmds" -o -e "$dir/$libname_spec.$libext"; then
+        if test -x "$_cfg" -a -r "$_cfg" -a -f "$_cfg"; then
           AC_MSG_RESULT([yes])
-
-          AC_MSG_CHECKING([$dir/$name usability])
-          ac_save_LIBS="$LIBS"
-          LIBS="$LIBS -L$dir"
-          AC_CHECK_LIB([$mysql_lib_name], [mysql_init], [
-            AC_MSG_RESULT([yes])
-            mysql_lib_path="$dir"
-          ], [
-            AC_MSG_RESULT([no])
-          ])
-          LIBS="$ac_save_LIBS"
-
-          if test ! -z "$mysql_lib_path"; then
-            break
-          fi
+          AC_DEFINE([HAVE_MYSQL_H], [1], [Define to `1' if you have the <mysql.h> header file.])
+          mysql_config="$_cfg"
+          break
         else
           AC_MSG_RESULT([no])
         fi
-      done
 
-      for dir in "$_pfx/include" "$_pfx/include/mysql"; do
-        AC_MSG_CHECKING([mysql headers availability in $dir])
-        if test -e "$dir/mysql.h"; then
-          AC_MSG_RESULT([yes])
-          AC_MSG_CHECKING([mysql headers usability])
-          ac_save_CPPFLAGS="$CPPFLAGS"
-          CPPFLAGS="$CPPFLAGS -I$dir"
-          AC_CHECK_HEADER([mysql.h], [
+        for dir in "$_pfx/lib" "$_pfx/lib/mysql"; do
+          AC_MSG_CHECKING([$mysql_lib_name availability in $dir])
+          name="$mysql_lib_name"
+
+          if eval test -e "$dir/$libname_spec$shrext_cmds" -o -e "$dir/$libname_spec.$libext"; then
             AC_MSG_RESULT([yes])
-            AC_DEFINE([HAVE_MYSQL_H], [1], [Define to `1' if you have the <mysql.h> header file.])
-            mysql_include_path="$dir"
-          ], [
+
+            AC_MSG_CHECKING([$dir/$name usability])
+            ac_save_LIBS="$LIBS"
+            LIBS="$LIBS -L$dir"
+            AC_CHECK_LIB([$mysql_lib_name], [mysql_init], [
+              AC_MSG_RESULT([yes])
+              mysql_lib_path="$dir"
+            ], [
+              AC_MSG_RESULT([no])
+            ])
+            LIBS="$ac_save_LIBS"
+
+            if test ! -z "$mysql_lib_path"; then
+              break
+            fi
+          else
             AC_MSG_RESULT([no])
-          ])
-          CPPFLAGS="$ac_save_CPPFLAGS"
+          fi
+        done
 
-          if test ! -z "$mysql_include_path"; then
-            break
+        for dir in "$_pfx/include" "$_pfx/include/mysql"; do
+          AC_MSG_CHECKING([mysql headers availability in $dir])
+          if test -e "$dir/mysql.h"; then
+            AC_MSG_RESULT([yes])
+            AC_MSG_CHECKING([mysql headers usability])
+            ac_save_CPPFLAGS="$CPPFLAGS"
+            CPPFLAGS="$CPPFLAGS -I$dir"
+            AC_CHECK_HEADER([mysql.h], [
+              AC_MSG_RESULT([yes])
+              AC_DEFINE([HAVE_MYSQL_H], [1], [Define to `1' if you have the <mysql.h> header file.])
+              mysql_include_path="$dir"
+            ], [
+              AC_MSG_RESULT([no])
+            ])
+            CPPFLAGS="$ac_save_CPPFLAGS"
+
+            if test ! -z "$mysql_include_path"; then
+              break
+            fi
+          else
+            AC_MSG_RESULT([no])
           fi
-        else
-          AC_MSG_RESULT([no])
-        fi
+        done
       done
-    done
-  fi
+    ])
+  ])
 
-  if test -z "$mysql_config"; then
+  if test "$mysql_pkg_config" = yes; then
+    CFLAGS="$CFLAGS $MYSQLCLIENT_CFLAGS"
+    LIBS="$LIBS $MYSQLCLIENT_LIBS"
+  elif test -z "$mysql_config"; then
     if test -z "$mysql_lib_path" -o -z "$mysql_include_path"; then
       AC_MSG_ERROR([Cannot locate mysql client library. Please check your mysql installation.])
     fi
